//
//  jonasViewController.h
//  Interns
//
//  Created by Jonas on 2015-03-27.
//  Copyright (c) 2015 Jonas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@interface jonasViewController : UIViewController

@property (strong, nonatomic) MPMoviePlayerController *moviePlayer;
@property (nonatomic) int imageOnTop;

@end
