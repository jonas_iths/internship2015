//
//  jonasViewController.m
//  Interns
//
//  Created by Jonas on 2015-03-27.
//  Copyright (c) 2015 Jonas. All rights reserved.
//

#import "jonasViewController.h"

@interface jonasViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UITextView *text;

@end

@implementation jonasViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // NSLog(@"imageontop: %d", self.imageOnTop);
    
    
    switch (self.imageOnTop) {
        case 1:
        {
            // self.text.text = @"I am Jonas! Developing apps is my flavour. Check out the video above or contact me: jonas@jonasekstrom.se or +46704097366. \nAs well I got some interesting things at www.jonasekstrom.se";
            
            NSURL *URL = [NSURL URLWithString: @"http://www.jonasekstrom.se"];
            NSMutableAttributedString * str = [[NSMutableAttributedString alloc] initWithString:@"I am Jonas! Developing apps is my flavour. Check out the video above or contact me: jonas@jonasekstrom.se or +46704097366. As well I got some interesting things at www.jonasekstrom.se"];
            [str addAttribute: NSLinkAttributeName value:URL range: NSMakeRange(str.length-19, 19)];
            self.text.attributedText = str;
            
            [self.image setImage:[UIImage imageNamed: @"jonas_play"] ];
        }
            break;
        case 2:
        {
            
            NSMutableAttributedString * str = [[NSMutableAttributedString alloc] initWithString:@"My name is Johan! I am 24 years old. Check out the video above or contact me: h4ggstr0m@hotmail.com.\n\nI have accepted an internship offer so I am currently unavailable."];
            
            [str addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(str.length-69,69)];
            self.text.attributedText = str;
            
            [self.image setImage:[UIImage imageNamed: @"johan_play"] ];
        }
            break;
        case 3:
        {
            NSURL *URL = [NSURL URLWithString: @"https://www.linkedin.com/profile/view?id=365386888&trk=hp-feed-view-profile"];
            NSMutableAttributedString * str = [[NSMutableAttributedString alloc] initWithString:@"Hi there, I'm Mattias. Currently I'm studying to become a developer for mobile applications. Check out my video above or contact me in some way. Mail: matte__88@hotmail.com Phone: +46763938009 or just check out my Linkedin Profile"];
            [str addAttribute: NSLinkAttributeName value:URL range: NSMakeRange(str.length-17, 17)];
            self.text.attributedText = str;
            
            [self.image setImage:[UIImage imageNamed: @"larm_play"] ];
        }
            break;
            
        default:
            break;
    }
    CGFloat yourSelectedFontSize = 16.0 ;
    UIFont *yourNewSameStyleFont = [self.text.font fontWithSize:yourSelectedFontSize];
    self.text.font = yourNewSameStyleFont ;
}

- (IBAction)closeButton:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)buttonPlayMovie:(id)sender {
    
    [self playMovie];
}

-(void)playMovie
{
    
    NSString *urlCase;
    
    switch (self.imageOnTop) {
        case 1:
            urlCase = @"jonas_low";
            break;
        case 2:
            urlCase = @"johan_low";
            break;
        case 3:
            urlCase = @"alarma_low";
            break;
            
        default:
            break;
    }
    
    
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle]
                                         pathForResource:urlCase ofType:@"mov"]];
    
    _moviePlayer =  [[MPMoviePlayerController alloc]
                     initWithContentURL:url];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:_moviePlayer];
    
    _moviePlayer.controlStyle = MPMovieControlStyleDefault;
    _moviePlayer.shouldAutoplay = YES;
    [self.view addSubview:_moviePlayer.view];
    [_moviePlayer setFullscreen:YES animated:NO];
}

- (void) moviePlayBackDidFinish:(NSNotification*)notification {
    MPMoviePlayerController *player = [notification object];
    [[NSNotificationCenter defaultCenter]
     removeObserver:self
     name:MPMoviePlayerPlaybackDidFinishNotification
     object:player];
    
    if ([player
         respondsToSelector:@selector(setFullscreen:animated:)])
    {
        [player.view removeFromSuperview];
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
