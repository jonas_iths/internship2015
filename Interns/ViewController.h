//
//  ViewController.h
//  Interns
//
//  Created by Jonas on 2015-02-21.
//  Copyright (c) 2015 Jonas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface ViewController : UIViewController <MFMailComposeViewControllerDelegate>


@end

