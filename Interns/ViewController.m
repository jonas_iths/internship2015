//
//  ViewController.m
//  Interns
//
//  Created by Jonas on 2015-02-21.
//  Copyright (c) 2015 Jonas. All rights reserved.
//

#import "ViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "jonasViewController.h"


@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *image3;

@property (weak, nonatomic) IBOutlet UIView *infoMailFrame;
@property (weak, nonatomic) IBOutlet UIImageView *image2;

@property (weak, nonatomic) IBOutlet UIImageView *image1;
@property(nonatomic) int screenWidth;
@property(nonatomic) int screenHeight;

@property(nonatomic) CALayer *containerLayer;
@property(nonatomic) CALayer *containerLayer2;
@property(nonatomic) CALayer *containerLayer3;

@property(nonatomic) BOOL freeze;

// YES = left, NO = right
@property(nonatomic) BOOL direction;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintInfoMail;
@property (weak, nonatomic) IBOutlet UIImageView *infoButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintInfoMailWidth;

@property float rotation;

@property int imageOnTop;

@property int once;


@end


@implementation ViewController

@synthesize image1;
@synthesize image2;
@synthesize image3;
@synthesize containerLayer;
@synthesize containerLayer2;
@synthesize containerLayer3;
@synthesize screenWidth;
@synthesize screenHeight;

- (IBAction)mailButton:(id)sender {
    
    [self displayComposerSheet];
}

-(void)displayComposerSheet
{
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    
    [picker setSubject:@"Internship"];
    
    // Set up the recipients.
    NSArray *toRecipients = [NSArray arrayWithObjects:@"swedishappdeveloperinterns@gmail.com",
                             nil];
    
    [picker setToRecipients:toRecipients];
    
    // Fill out the email body text.
    NSString *emailBody = @"";
    [picker setMessageBody:emailBody isHTML:NO];
    
    // Present the mail composition interface.
    [self presentViewController:picker animated:YES completion:nil];
}

// The mail compose view controller delegate method
- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error
{
    
    [controller dismissViewControllerAnimated:YES completion:nil];
}



-(void)viewDidLayoutSubviews
{
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    self.once += 1;
    self.constraintInfoMail.constant = self.infoMailFrame.frame.size.width / 2;
    
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    screenWidth = (int)screenSize.width;
    screenHeight = (int)screenSize.height;
    
    if (self.once < 2) {
        
        // NSLog(@"SUBLAYOYTviews");
        [self imagesSetup];
        
    }
}

- (IBAction)buttonToPerson:(id)sender {
    
    [self performSegueWithIdentifier: @"jonasSegue" sender: self];
    
}

-(void)viewDidAppear:(BOOL)animated {
    
    if (self.once < 2) {
        // NSLog(@"appear");
        self.once += 1;
        // NSLog(@"once = %d", self.once);
        
        image1.center = CGPointMake(self.view.center.x, self.view.center.y);
        image2.center = CGPointMake(image2.center.x, self.view.center.y + screenHeight * 0.025);
        image3.center = CGPointMake(self.view.center.x, self.view.center.y + screenHeight * 0.05);
        
    }
}

-(void)imagesSetup
{
    
    // NSLog(@"imagesSetup");
    
    image1.transform = CGAffineTransformMakeScale(1, 1);
    // image1.center = CGPointMake(self.view.center.x, self.view.center.y);
    
    image2.transform = CGAffineTransformMakeScale(0.95, 0.95);
    // image2.center = CGPointMake(self.view.center.x, self.view.center.y+ screenHeight * 0.05);
    
    image3.transform = CGAffineTransformMakeScale(0.9, 0.9);
    // image3.center = CGPointMake(self.view.center.x, self.view.center.y + screenHeight * 0.025);
    
    // image 1
    
    [image1.layer setBorderColor: [[UIColor grayColor] CGColor]];
    [image1.layer setBorderWidth: 1.0];
    
    containerLayer = [CALayer layer];
    containerLayer.shadowColor = [UIColor blackColor].CGColor;
    containerLayer.shadowRadius = 3.0;
    containerLayer.shadowOffset = CGSizeMake(2,4);
    containerLayer.shadowOpacity = 0.2;
    
    // use the image's layer to mask the image into a circle
    image1.layer.cornerRadius = 10.0;
    image1.layer.masksToBounds = YES;
    
    // UIButton *button1;
    
    // add masked image layer into container layer so that it's shadowed
    [containerLayer addSublayer:image1.layer];
    
    
    // add container including masked image and shadow into view
    [self.view.layer addSublayer:containerLayer];
    
    ////////////////////////////////////////////////////////////////////////
    
    // image 2
    
    [image2.layer setBorderColor: [[UIColor grayColor] CGColor]];
    [image2.layer setBorderWidth: 1.0];
    
    containerLayer2 = [CALayer layer];
    containerLayer2.shadowColor = [UIColor blackColor].CGColor;
    containerLayer2.shadowRadius = 3.0;
    containerLayer2.shadowOffset = CGSizeMake(2,4);
    containerLayer2.shadowOpacity = 0.2;
    
    // use the image's layer to mask the image into a circle
    image2.layer.cornerRadius = 10.0;
    image2.layer.masksToBounds = YES;
    
    // add masked image layer into container layer so that it's shadowed
    [containerLayer2 addSublayer:image2.layer];
    
    // add container including masked image and shadow into view
    [self.view.layer addSublayer:containerLayer2];
    
    ////////////////////////////////////////////////////////////////////////
    
    // image 3
    
    [image3.layer setBorderColor: [[UIColor grayColor] CGColor]];
    [image3.layer setBorderWidth: 1.0];
    
    containerLayer3 = [CALayer layer];
    containerLayer3.shadowColor = [UIColor blackColor].CGColor;
    containerLayer3.shadowRadius = 3.0;
    containerLayer3.shadowOffset = CGSizeMake(2,4);
    containerLayer3.shadowOpacity = 0.2;
    
    // use the image's layer to mask the image into a circle
    image3.layer.cornerRadius = 10.0;
    image3.layer.masksToBounds = YES;
    
    // add masked image layer into container layer so that it's shadowed
    [containerLayer3 addSublayer:image3.layer];
    
    // add container including masked image and shadow into view
    [self.view.layer addSublayer:containerLayer3];
    
    
    
    switch (self.imageOnTop) {
        case 1:
            containerLayer.zPosition = 30;
            containerLayer2.zPosition = 29;
            containerLayer3.zPosition = 28;
            break;
        case 2:
            containerLayer.zPosition = 28;
            containerLayer2.zPosition = 30;
            containerLayer3.zPosition = 29;
            break;
        case 3:
            containerLayer.zPosition = 29;
            containerLayer2.zPosition = 28;
            containerLayer3.zPosition = 30;
            break;
    }
    
    self.infoMailFrame.layer.zPosition = -100;
    
}




- (IBAction)dragging:(UIPanGestureRecognizer*)sender {
    
    UIImageView *actualImage;
    UIImageView *secondImage;
    UIImageView *thirdImage;
    
    switch (self.imageOnTop) {
        case 1:
            actualImage = image1;
            secondImage = image2;
            thirdImage = image3;
            break;
        case 2:
            actualImage = image2;
            secondImage = image3;
            thirdImage = image1;
            break;
        case 3:
            actualImage = image3;
            secondImage = image1;
            thirdImage = image2;
            break;
    }
    
    CGPoint translation = [sender translationInView:self.view];
    
    if (self.freeze == NO) {
        
        actualImage.center = CGPointMake(actualImage.center.x + translation.x, actualImage.center.y + translation.y);
        
        float rot = (actualImage.center.x - self.view.center.x) * 0.001f;
        // NSLog(@"Rot: %f", rot);
        
        CGAffineTransform newTransform = CGAffineTransformMakeRotation(rot);
        
        actualImage.transform = newTransform;
        
    }
    
    if([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateEnded) {
        
        // check if outside
        if (actualImage.frame.origin.x < 0 - screenWidth/4 || actualImage.frame.origin.x + actualImage.frame.size.width > screenWidth + screenWidth/4) {
            
            self.freeze = YES;
            
            int directX;
            
            if (actualImage.frame.origin.x < 0 - screenWidth/4) {
                self.direction = YES;
                directX = -1;
            } else {
                self.direction = NO;
                directX = 1;
            }
            
            [UIView animateWithDuration:0.20
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseOut
                             animations:^{
                                 
                                 actualImage.center = CGPointMake(self.view.frame.size.width * directX * 2, image1.center.y + self.view.frame.size.height / 2);
                                 
                                 actualImage.alpha = 0;
                                 
                                 int deegre;
                                 
                                 if (self.direction) {
                                     deegre = 300;
                                 } else {
                                     deegre = 45;
                                 }
                                 // NSLog(@"deegre: %d", deegre);
                                 actualImage.transform =  CGAffineTransformMakeRotation((deegre) / 180.0 * M_PI);
                                 
                                 [UIView beginAnimations:nil context:nil];
                                 [UIView setAnimationDuration:0.1];
                                 [UIView setAnimationDelay:0.0];
                                 [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
                                 
                                 secondImage.transform = CGAffineTransformMakeScale(1, 1);
                                 secondImage.center = CGPointMake(self.view.center.x, self.view.center.y );
                                 
                                 [UIView commitAnimations];
                                 
                                 [UIView beginAnimations:nil context:nil];
                                 [UIView setAnimationDuration:0.1];
                                 [UIView setAnimationDelay:0.0];
                                 [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
                                 
                                 thirdImage.transform = CGAffineTransformMakeScale(0.95, 0.95);
                                 thirdImage.center = CGPointMake(self.view.center.x, self.view.center.y + (screenHeight * 0.025) );
                                 
                                 
                                 [UIView commitAnimations];
                                 
                             }
                             completion:^(BOOL finished){
                                 
                                 // change z order in switch
                                 // change frame sizes
                                 
                                 actualImage.transform =  CGAffineTransformMakeRotation(0 / 180.0 * M_PI);
                                 
                                 switch (self.imageOnTop) {
                                     case 1:
                                         self.imageOnTop = 2;
                                         containerLayer.zPosition = 28;
                                         containerLayer2.zPosition = 30;
                                         containerLayer3.zPosition = 29;
                                         break;
                                     case 2:
                                         self.imageOnTop = 3;
                                         containerLayer.zPosition = 29;
                                         containerLayer2.zPosition = 28;
                                         containerLayer3.zPosition = 30;
                                         break;
                                     case 3:
                                         self.imageOnTop = 1;
                                         containerLayer.zPosition = 30;
                                         containerLayer2.zPosition = 29;
                                         containerLayer3.zPosition = 28;
                                         break;
                                 }
                                 
                                 actualImage.transform = CGAffineTransformMakeScale(0.9, 0.9);
                                 
                                 actualImage.center = CGPointMake(self.view.center.x, self.view.center.y+ (screenHeight * 0.050) );
                                 
                                 self.freeze = NO;
                                 
                                 [UIView beginAnimations:nil context:nil];
                                 [UIView setAnimationDuration:0.3];
                                 [UIView setAnimationDelay:0.1];
                                 [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
                                 
                                 actualImage.alpha = 1;
                                 
                                 [UIView commitAnimations];
                                 
                             }];
            
            self.infoMailFrame.layer.zPosition = -100;
            
            // else animate back
        } else {
            
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.3];
            [UIView setAnimationDelay:0.0];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
            
            actualImage.center = CGPointMake(self.view.center.x, self.view.center.y);
            actualImage.transform =  CGAffineTransformMakeRotation((0) / 180.0 * M_PI);
            
            [UIView commitAnimations];
            
        }
        
    }
    
    [sender setTranslation:CGPointMake(0,0) inView:self.view];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.once = 0;
    self.imageOnTop = 1;
    // NSLog(@"viewdidload");
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"jonasSegue"]) {
        
        jonasViewController *vc = [segue destinationViewController];
        
        // Pass any objects to the view controller here, like...
        vc.imageOnTop = self.imageOnTop;
        
    }
}


@end
